package main

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestServeRequestGet(t *testing.T) {
	for _, url := range []string{"/", "/foo/bar"} {
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Errorf("Unexpected error: %s", err)
		}
		w := httptest.NewRecorder()

		serveRequest(w, req)
		contentTypeHeader := w.Header().Get("Content-Type")

		assert.Equal(t, http.StatusOK, w.Code, "Expected response code %d, got: %d", http.StatusOK, w.Code)
		assert.Equal(t, "Welcome to the COYO DevOps Game!", w.Body.String(), "Expected response  'OK', got: %s", w.Body.String())
		assert.Equal(t, "text/plain; charset=utf-8", contentTypeHeader, "Expected header 'Content-Type' to be 'text/plain; charset=utf8', got: %s", contentTypeHeader)
	}
}

func TestServeRequestPost(t *testing.T) {
	for _, url := range []string{"/", "/foo/bar"} {
		req, err := http.NewRequest("POST", url, nil)
		if err != nil {
			t.Errorf("Unexpected error: %s", err)
		}
		w := httptest.NewRecorder()

		serveRequest(w, req)

		assert.Equal(t, http.StatusMethodNotAllowed, w.Code, "Expected response code %d, got: %d", http.StatusMethodNotAllowed, w.Code)
	}
}

func TestIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping this test since running in short mode.")
	}

	go listen(8099)
	rsp, err := http.Get("http://localhost:8099/blubb")
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}

	assert.Equal(t, http.StatusOK, rsp.StatusCode, "Expected response code %d, got: %d", http.StatusOK, rsp.StatusCode)
}
