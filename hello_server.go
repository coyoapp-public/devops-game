package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

func formatRequest(method, path string, statusCode int) string {
	return fmt.Sprintf("%s request to %s with status code %d", method, path, statusCode)
}

func serveRequest(w http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		logrus.Info(formatRequest(req.Method, req.URL.Path, http.StatusMethodNotAllowed))
		w.Header().Add("Allow", "GET")
		http.Error(w, "Only GET supported.", http.StatusMethodNotAllowed)
		return
	}

	logrus.Info(formatRequest(req.Method, req.URL.Path, http.StatusOK))
	w.Header().Add("Content-Type", "text/plain; charset=utf-8")
	io.WriteString(w, "Welcome to the COYO DevOps Game!")
}

func listen(port uint) error {
	logrus.Info("Listening on port ", port)
	addr := fmt.Sprintf(":%d", port)
	http.Handle("/", http.HandlerFunc(serveRequest))
	if err := http.ListenAndServe(addr, nil); err != nil {
		return err
	}
	logrus.Info("Stopped http server")
	return nil
}

func main() {
	err := listen(8080)
	if err != nil {
		logrus.Fatalf("Error starting HTTP server: %s", err)
	}
}
