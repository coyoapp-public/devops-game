module gitlab.com/coyoapp-public/devops-game

go 1.15

require (
	github.com/stretchr/testify v1.6.1
	github.com/sirupsen/logrus v1.6.0
)
