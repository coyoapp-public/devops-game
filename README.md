# COYO DevOps Exercise

## Prerequisites
* [Docker](https://www.docker.com/)
* [Golang](https://golang.org/) (if you want to run the application locally)

## Tasks
1. Clone this repository
2. Move to the local repository
3. Create a multistage `Dockerfile` that build the application and then copies the artifact to its final image
   
   For downloading the dependencies, running tests and building the artifact you can use following commands:
```
# Download dependencies
go mod download

# Run tests
CGO_ENABLED=0 go test -v 

# Build artifacts
go build -o <OUTPUT FILENAME> . 
```

4. Create a `docker-compose.yaml` that runs your image.
5. Create a ```.gitlab-ci.yaml```. It should
    1. use ```coyoapp/${CI_PROJECT_NAME}``` as the image name
    2. login, build and push the image tagged with ``${CI_COMMIT_REF_SLUG}`` to dockerhub (Dockerhub credentials: ```${DOCKERHUB_USER}``` and ```${DOCKERHUB_PASSWORD}```. Those are part of the repository already.)
